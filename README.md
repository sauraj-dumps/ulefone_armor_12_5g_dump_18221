## full_k6833v1_64-user 11 RP1A.200720.011 1628831074 release-keys
- Manufacturer: ulefone
- Platform: mt6833
- Codename: Armor_12_5G
- Brand: Ulefone
- Flavor: full_k6833v1_64-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: 1628831074
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Ulefone/Armor_12_5G/Armor_12_5G:11/RP1A.200720.011/1628831074:user/release-keys
- OTA version: 
- Branch: full_k6833v1_64-user-11-RP1A.200720.011-1628831074-release-keys
- Repo: ulefone_armor_12_5g_dump_18221


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
